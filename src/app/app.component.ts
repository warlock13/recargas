import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'recargas';
  operadores: any[] = [];
  usuarios: any[] = [];

  operadorID: number | null = null;
  userID: number | null = null;
  valor: number | null = null;

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.cargarOperadores();
    this.cargarUsuarios();
  }

  cargarOperadores() {
    this.http.get<any[]>('http://localhost:8080/operadores')
      .subscribe(data => {
        this.operadores = data;
      });
  }
  cargarUsuarios() {
    this.http.get<any[]>('http://localhost:8080/usuarios')
      .subscribe(data => {
        this.usuarios = data;
      });
  }
  enviarTransaccion() {
    if (this.operadorID === null || this.userID === null || this.valor === null) {
      return;
    }
    const datosTransaccion = {
      operadorID: this.operadorID,
      userID: this.userID,
      valor: this.valor
    };

    this.http.post('http://localhost:8080/transaccion', datosTransaccion)
      .subscribe(response => {
        console.log('Solicitud POST exitosa', response);
      });
  }
  
}
